#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Libre.h"

void main(void) {
    int opcion=1;
    int cantidad=0;
    int j=0;
    printf("Aplicacion de alumnos\n");
    while(opcion != 0)
  {
        opcion=menu();

        switch (opcion) {
          case 0:
          printf("\n\aAdios\n" );
          break;
            case 1:
                 printf("Crear una nueva lista de alumnos\n");
                 printf("Cuantos alumnos quieres crear?\n");
                 scanf("%d",&cantidad);
                 for(j=0;j<cantidad;j++){
                   printf("Guardar datos del alumno %d\n",j+1);
                   nuevoAlumno();
                 }
                break;
            case 2:
                printf("Guardar lista al archivo\n");
                grabaRegistros(listaAlumnos,indiceAlArreglo);
                break;
            case 3:
                printf("Leer la lista desde el archivo\n");
                indiceAlArreglo=registrosEnArchivo();
                leerRegistros(indiceAlArreglo);
                break;
            case 4:
                printf("Mostrar todos los datos de la lista\n");
                imprimirLista();
                break;
            case 5:
                printf("Agregar un nuevo alumno a la lista\n");
                nuevoAlumno();
                break;
			case 6:
			printf("Obtener promedio de alumnos.\n");
			imprimirpromedio();
			break;
			case 7:
			printf("Buscar alumno por nombre.\n");
			busnom();
			break;
		    case 8:
			printf("Buscar alumno por edad.\n");
			Bused();
			break;
		    case 9:
			printf("Eliminar alumno.\n");
			eliminaralum();
			break;
		    case 10:
			printf("Modificar datos.\n");
			modificardatos();
			break;
            default:
                printf("\n\aLa opcion seleccionada no esta en el menu\n");
                opcion = 0;
                break;
        }
    }

}
