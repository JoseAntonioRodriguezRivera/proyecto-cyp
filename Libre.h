#define MAX 180
#ifndef alumnos_h
#define alumnos_h
struct alumnos
{
    int edad;
    char nombre[120];
    char genero;
    char carrera[50];
    char nCuenta[10];
    float promedio;
};
typedef struct alumnos ALUMNO;

int indiceAlArreglo=0;
ALUMNO listaAlumnos[MAX];
void insertarAlumno(ALUMNO al){
    if (indiceAlArreglo >= 0 && indiceAlArreglo < MAX) {
        listaAlumnos[indiceAlArreglo]=al;
        indiceAlArreglo++;
    }else{
        printf("El indice apunta fuera del arreglo, favor de revisar la logica");
    }
}


ALUMNO nuevoAlumno(){
    ALUMNO tmp;
    printf("Introduce la edad:");
    scanf("%d",&tmp.edad);
    printf("Introduce el genero [M o F]:");
    scanf(" %c",&tmp.genero);
    printf("Introduce el nombre:");
    scanf("%*c%[^\n]",tmp.nombre);
    printf("Introduce Carrera:");
    scanf("%*c%[^\n]",tmp.carrera);
    printf("Introduce numero de cuenta:");
    scanf("%*c%[^\n]",tmp.nCuenta);
    printf("Introduce tu promedio:");
    scanf("%f",&tmp.promedio);

    insertarAlumno(tmp);
    return tmp;
}

void imprimeAlumno(ALUMNO alu){
    printf("\tNombre:%s\n",alu.nombre);
    printf("\tEdad:%d\n",alu.edad);
    printf("\tGenero:%c\n",alu.genero);
    printf("\tCarrera:%s\n",alu.carrera);
    printf("\tNumero de Cuenta:%s\n",alu.nCuenta);
    printf("\tPromedio:%.2f\n",alu.promedio);
    printf("+---------------------------------+\n\n");
}

void imprimirLista(){
    int j=0;
    for (j = 0; j < indiceAlArreglo; j++) {
        printf("+--------- Numero de lista: %d ---------+*\n",j+1);
        imprimeAlumno(listaAlumnos[j]);
    }
}



int menu(){
    int op=0;
    printf("\n----------- Menu para la aplicacion de BD para alumnos ---------\n");
    printf("(0) SALIR\n");
    printf("(1) Crear lista.\n");
    printf("(2) Guardar a archivo.\n");
    printf("(3) Leer desde archivo.\n");
    printf("(4) Mostrar lista.\n");
    printf("(5) Agregar alumno. \n");
    printf("(6) Obtener promedio de alumnos.\n");
    printf("(7) Buscar alumno por nombre. \n");
    printf("(8) Buscar alumno por edad. \n");
    printf("(9) Eliminar alumno. \n");
    printf("(10) Modificar datos \n");
    printf("\n\nElige una opcion:");
    scanf("%d",&op);
    printf("\n\n" );

    return op;
}
void imprimirpromedio(){
	int i;
	float ipromedio=0.0;
	float gpromedio=0.0;
	for(i=0; i<=indiceAlArreglo; i++){
		ipromedio=ipromedio+listaAlumnos[i].promedio;
	}
	gpromedio=ipromedio/indiceAlArreglo;
	printf("Promedio: %.3f",gpromedio);
}
void busnom()
{
	char nombre[20];
	int i=0;


	printf("Dame el nombre a buscar: ");
	scanf("%s",nombre);
  printf("\n" );
	for(i=0; i<indiceAlArreglo; i++){
		if(strcmp(nombre,listaAlumnos[i].nombre)==0){

			imprimeAlumno(listaAlumnos[i]);

		}else{
			printf("\n %s no existe:  \n\n",nombre);

		}}
}

void Bused(){
	int edad;
	int i=0;
	printf("Ingrese la edad del alumno:");
	scanf("%d",&edad);
	for(i=0; i<indiceAlArreglo; i++){
		if(edad==listaAlumnos[i].edad){
			imprimeAlumno(listaAlumnos[i]);
		}else {

			printf("\n Nadie tiene: %d años \n",edad);
		}
	}

}
void eliminaralum(){
	int i;
	int gelimi;
	printf("Que numero de lista deseas eliminar?:");
	scanf("%i",&gelimi);
	for(i=gelimi-1; i<=indiceAlArreglo; i++){
		listaAlumnos[i]=listaAlumnos[i];
	}
	indiceAlArreglo=indiceAlArreglo-1;
}
void modificardatos(){
	int i;
	int numero;
	int datos;

	printf("Que numero de lista deseas modificar?:");
	scanf("%d",&numero);
	for(i=numero-1; i<=indiceAlArreglo; i++){
		listaAlumnos[i]=listaAlumnos[numero-1];
	}
	imprimeAlumno(listaAlumnos[i]);
	printf("-----elige una opcion-----\n");
	printf("(1) nombre\n");
	printf("(2) edad\n");
	printf("(3) carrera\n");
	printf("(4) nCuenta\n");
	printf("(5) promedio");
	printf("\nelige una opcion:");
	scanf("%d",&datos);

	switch(datos){
	case 1:
		printf("El nombre es: %s",listaAlumnos[numero-1].nombre);
		printf("\nEscribe el nuevo nombre:");
		scanf("%s",&listaAlumnos[numero-1].nombre);
		printf("\nEl nombre es: %s",listaAlumnos[numero-1].nombre);

		break;
	case 2:
		printf("\nLa edad es: %i\n",listaAlumnos[numero-1].edad);
		printf("Introduce la nueva edad:");
		scanf("%i\n",listaAlumnos[numero-1].edad);
		printf("\nLa edad es:&i",listaAlumnos[numero-1]);
		break;
	case 3:
		printf("\nla carrera es: %s",listaAlumnos[numero-1].carrera);
		printf("\nIntroduce la nueva carrera:");
		scanf("%s",listaAlumnos[numero-1].carrera);
		printf("\nnueva carrera es: %s",listaAlumnos[numero-1]);
		break;
	case 4:
		printf("\nEl nCuenta es: %d",listaAlumnos[numero-1].nCuenta);
		printf("\nintroduce el nuevo nCuenta:");
		scanf("%d",&listaAlumnos[numero-1].nCuenta);
		printf("\nel nCuenta nuevo es: %f",listaAlumnos[numero-1]);
		break;
	case 5:
		printf("\nEl promedio es: %.f",listaAlumnos[numero-1].promedio);
		printf("\nintroduce el nuevo promedio:");
		scanf("%.f",&listaAlumnos[numero-1].promedio);
		printf("\nel nuevo promedio es:%.f ",listaAlumnos[numero-1]);
		break;
	}
}

void grabaRegistros(ALUMNO r[], int tam){
    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","w"))==NULL){
        printf("el archivo no se puede abrir\n");
    }else{
        fwrite(r,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

void leerRegistros(int tam){

    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("El archivo no se puede abrir\n");
    }
    else{
        //for /*(int i=0;i<tam;i++)*/
        fread(listaAlumnos,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

int registrosEnArchivo(){
    FILE *ptrF;
    int contador=0;
    ALUMNO  basura;
    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("el archivo no se puede abrir\n");
    }
    else{
        while(!feof(ptrF)){
            if (fread(&basura,sizeof(ALUMNO),1,ptrF))
                contador++;
        }

    }
    fclose(ptrF);
    return contador;
}
#endif /* alumnos_h */
